# Random Doom Lan Party Site

A fan-made web front-end for the 'Obsidian Level Maker' tool. This is written to be used alongside the corresponding [backend](https://gitlab.com/dreamur1/doom-wotm-service).

## Example/ Demo

![Looping gif of a site demo](src/assets/siteDemo.gif)


## Running it yourself

As with any other angular app, use `ng serve` or `npm start`. The site is configured to run on angular's default port of `4200`. 

## Credits / Dependencies

- [Angular CLI](https://github.com/angular/angular-cli) version 16.2.8
- [NodeJs](https://nodejs.org/en) version 20.9.0
- [PrimeNg](https://primeng.org) version 16.9.1
- [RxStomp](https://github.com/stomp-js/rx-stomp) version 2.0.0
