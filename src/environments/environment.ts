export const environment = {
    apiUrl: "https://api.randomdoomlan.party",
    wsUrl: "wss://api.randomdoomlan.party/cable"
};
