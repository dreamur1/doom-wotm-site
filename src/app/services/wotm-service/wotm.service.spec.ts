import { TestBed } from '@angular/core/testing';

import { WotmService } from './wotm.service';
import { HttpClientModule } from '@angular/common/http';

describe('WotmService', () => {
  let service: WotmService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
      ],
    });
    service = TestBed.inject(WotmService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
