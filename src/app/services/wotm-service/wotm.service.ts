import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ReplaySubject, firstValueFrom, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ObsidianConfigService } from '../obsidian-config-service/obsidian-config.service';
import { ObsidianConfig } from 'src/app/models/obsidianConfig';
import * as ActionCable from 'actioncable';

export class WadData {
  wadName: string = '';
  deletionTimestamp: string = '';
  wasUserRequested: boolean = false;
  fontSize: string = '';
  obsidianConfig: ObsidianConfig = new ObsidianConfig();
}

@Injectable({
  providedIn: 'root'
})
export class WotmService {
  static availableWadNames$: ReplaySubject<WadData[]> = new ReplaySubject<WadData[]>(1);
  static wotms$: ReplaySubject<WadData[]> = new ReplaySubject<WadData[]>(1);
  static availableWadNames: WadData[] = [];
  static wotms: WadData[] = [];
  static userSubmittedWadNames: string[] = [];

  baseUrl: string = environment.apiUrl;
  retrievedConfig$: ReplaySubject<ObsidianConfig> = new ReplaySubject<ObsidianConfig>(1);
  formType$: ReplaySubject<string> = new ReplaySubject<string>(1);

  cable: ActionCable.Cable;

  constructor(private httpClient: HttpClient, private obsidianConfigService: ObsidianConfigService) { 
    let stringifiedNames = localStorage.getItem("requested-doom2-wad-names");

    if (stringifiedNames !== null) {
      WotmService.userSubmittedWadNames = JSON.parse(stringifiedNames) as string[];
    }

    this.cable = ActionCable.createConsumer(environment.wsUrl)

    this.cable.subscriptions.create('WotmChannel', {
        received(wadConfig: ObsidianConfig) {
          let newWad = {
            wadName: wadConfig.wadName,
            deletionTimestamp: wadConfig.deletionTimestamp,
            wasUserRequested: false,
            fontSize: 'larger',
            obsidianConfig: wadConfig
          };

          if (WotmService.userSubmittedWadNames.includes(newWad.wadName)) {
            newWad.wasUserRequested = true;
          }
          newWad.fontSize = newWad.wadName.length > 10 ? 'larger' : 'xx-large';
          WotmService.availableWadNames = [newWad, ...WotmService.availableWadNames];
          WotmService.availableWadNames$.next(WotmService.availableWadNames);
        }
    });

    this.fetchWotms();
    this.fetchAvailableWads();
  }

  async fetchAvailableWads(): Promise<void> {
    if (WotmService.availableWadNames.length > 0) return;
    WotmService.availableWadNames = await firstValueFrom(this.httpClient.get<WadData[]>(`${this.baseUrl}/wotm/available`));
    WotmService.availableWadNames.forEach((wad) => {
      this.isWadUserSubmitted(wad);
      this.determineFontSize(wad);
    });
    WotmService.availableWadNames$.next(WotmService.availableWadNames);
  }

  async fetchWotms(): Promise<void> {
    if (WotmService.wotms.length > 0) return;
    WotmService.wotms = await firstValueFrom(this.httpClient.get<WadData[]>(`${this.baseUrl}/wotm/current`));
    WotmService.wotms.forEach((wad) => {
      this.determineFontSize(wad);
    });
    WotmService.wotms$.next(WotmService.wotms);
  }

  async fetchConfig(targetWad: string, isWotm: boolean = false): Promise<void> {
    
    let foundWad = WotmService.availableWadNames.find((wad) => wad.wadName === targetWad);
    if (isWotm) {
      foundWad = WotmService.wotms.find((wad) => wad.wadName === targetWad);
    }

    if (!foundWad) { return; }

    this.formType$.next(foundWad.obsidianConfig.formType as string);
    this.retrievedConfig$.next(foundWad.obsidianConfig as ObsidianConfig);
  }

  async submitWadRequest(wadName: string, configFile: ObsidianConfig): Promise<void> {
    WotmService.userSubmittedWadNames.push(wadName);
    localStorage.setItem("requested-doom2-wad-names", JSON.stringify(WotmService.userSubmittedWadNames));

    let configTextFile = this.obsidianConfigService.buildObsidianConfigFile(wadName, configFile);
    console.log(configTextFile);
    await firstValueFrom(this.httpClient.post(`${this.baseUrl}/wotm/name/${wadName}`, configTextFile));
  }

  getUserSubmittedWadNames(): string[] {
    return WotmService.userSubmittedWadNames;
  }

  isWadUserSubmitted(targetWad: WadData): void {
    if (WotmService.userSubmittedWadNames.includes(targetWad.wadName)) {
      targetWad.wasUserRequested = true;
    }
  }

  determineFontSize(targetWad: WadData): void {
    targetWad.fontSize = targetWad.wadName.length > 10 ? 'larger' : 'xx-large';
  }
}
