import { TestBed } from '@angular/core/testing';

import { ObsidianConfigService } from './obsidian-config.service';

describe('ObsidianConfigService', () => {
  let service: ObsidianConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObsidianConfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
