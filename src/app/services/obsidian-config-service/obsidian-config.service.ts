import { Injectable } from '@angular/core';
import { ObsidianConfig } from '../../models/obsidianConfig';

@Injectable({
  providedIn: 'root'
})
export class ObsidianConfigService {

  constructor() { }

  buildObsidianConfigFile(wadName: string, obsidianConfig: ObsidianConfig): string {
    let txtFile = `-- ${wadName}\n-- formType = ${obsidianConfig.formType}`;
    
    let gameSettings = `
    \n---- Game Settings ----\n
    engine = idtech_1
    game = ${obsidianConfig.sourceWad}
    port = zdoom
    length = ${obsidianConfig.gameLength}
    theme = original`;

    let architectureSettings = `
    \n---- Architecture ----\n
    float_size = ${obsidianConfig.levelSize * 0.36}
    bool_prebuilt_levels = ${obsidianConfig.allowPrebuiltLevels ? 1 : 0}
    float_overall_lighting_mult = ${obsidianConfig.lightingMultiplier / 100}
    outdoors = ${obsidianConfig.outdoorsFrequency}
    caves = ${obsidianConfig.cavesFrequency}
    liquids = ${obsidianConfig.liquidsFrequency}
    hallways = ${obsidianConfig.hallwaysFrequency}
    teleporters = ${obsidianConfig.teleportersFrequency}
    zdoom_vista = ${obsidianConfig.enableBottomlessVistasAndSkyboxes ? 'enable' : 'disable'}
    zdoom_skybox = ${obsidianConfig.enableBottomlessVistasAndSkyboxes ? 'random' : 'disable'}`;

    let combatSettings = `
    \n---- Combat ----\n
    float_mons = ${obsidianConfig.monsterQuantity / 100}
    float_strength = ${obsidianConfig.monsterStrength / 100}
    bool_pistol_starts = ${obsidianConfig.pistolStartCompatable ? 1 : 0}
    bool_quiet_start = ${obsidianConfig.safeStartingRooms ? 1 : 0}
    mon_variety = ${obsidianConfig.monsterVariety}
    traps = ${obsidianConfig.trapQuantity}
    secret_monsters = ${obsidianConfig.monstersInSecrets ? 'yesyes' : 'no'}`;

    let pickupSettings = `
    \n---- Pickups ----\n
    health = ${obsidianConfig.healthPickupQuantity}
    ammo = ${obsidianConfig.ammoPickupQuantity}
    items = ${obsidianConfig.miscItemQuantity}
    secrets = ${obsidianConfig.secretsQuantity}
    secrets_bonus = ${obsidianConfig.secretsBonus}`;

    let otherSettings = `
    \n---- Other Modules ----\n
    @doom_mon_control = 1
      float_zombie = ${obsidianConfig.zombieCount / 5}
      float_shooter = ${obsidianConfig.shotgunnerCount / 5}
      float_gunner = ${obsidianConfig.sargeCount / 5}
      float_ss_nazi = ${obsidianConfig.naziCount / 5}
      float_imp = ${obsidianConfig.impCount / 5}
      float_skull = ${obsidianConfig.lostSoulCount / 5}
      float_demon = ${obsidianConfig.demonCount / 5}
      float_spectre = ${obsidianConfig.spectreCount / 5}
      float_pain = ${obsidianConfig.painElementalCount / 5}
      float_caco = ${obsidianConfig.cacodemonCount / 5}
      float_knight = ${obsidianConfig.hellKnightCount / 5}
      float_revenant = ${obsidianConfig.revenantCount / 5}
      float_mancubus = ${obsidianConfig.mancubusCount / 5}
      float_arach = ${obsidianConfig.arachnatronCount / 5}
      float_vile = ${obsidianConfig.archVileCount / 5}
      float_baron = ${obsidianConfig.baronOfHellCount / 5}
      float_Cyberdemon = ${obsidianConfig.cyberDemonCount / 5}
      float_Spiderdemon = ${obsidianConfig.spiderMastermindCount / 5}`;

    if (obsidianConfig.formType === 'basic') {
      return txtFile.concat(gameSettings, architectureSettings, combatSettings, pickupSettings);
    } 
    return txtFile.concat(gameSettings, architectureSettings, combatSettings, pickupSettings, otherSettings);
  }
}
