import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { KnobModule } from 'primeng/knob';
import { InputTextModule } from 'primeng/inputtext';
import { SelectButtonModule } from 'primeng/selectbutton';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { ToastModule } from 'primeng/toast';
import { PaginatorModule } from 'primeng/paginator';
import { GalleriaModule  } from 'primeng/galleria';
import { BadgeModule } from 'primeng/badge';
import { InputSwitchModule } from 'primeng/inputswitch';
import { TabViewModule } from 'primeng/tabview';
import { CheckboxModule } from 'primeng/checkbox';
import { SliderModule } from 'primeng/slider';
import { ProgressSpinnerModule } from 'primeng/progressspinner';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { AvailableWadsComponent } from './components/available-wads/available-wads.component';
import { CountdownPipe } from './pipes/countdown.pipe';
import { MessageService } from 'primeng/api';
import { BasicRequestFormComponent } from './components/basic-request-form/basic-request-form.component';
import { AdvancedRequestFormComponent } from './components/advanced-request-form/advanced-request-form.component';
import { LandingComponent } from './components/landing/landing.component';
import { RequestFormWrapperComponent } from './components/request-form-wrapper/request-form-wrapper.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AvailableWadsComponent,
    CountdownPipe,
    BasicRequestFormComponent,
    AdvancedRequestFormComponent,
    LandingComponent,
    RequestFormWrapperComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    KnobModule,
    InputTextModule,
    SelectButtonModule,
    ButtonModule,
    CardModule,
    ToastModule,
    PaginatorModule,
    GalleriaModule,
    BadgeModule,
    InputSwitchModule,
    TabViewModule,
    CheckboxModule,
    SliderModule,
    ProgressSpinnerModule
  ],
  providers: [
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
