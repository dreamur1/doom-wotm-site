import { Component } from '@angular/core';
import { WotmService } from './services/wotm-service/wotm.service';
import { map } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  availableTabs: any[] = [
    {label: 'FAQ', value: 'landing'}, 
    {label: 'Basic Mode', value: 'basicForm'}, 
    {label: 'Advanced Mode', value: 'advancedForm'},
    {label: 'WAD Downloads', value: 'downloads'},
  ];
  activeTab = "landing";
  title = 'doom-wotm-site';
}
