import { ObsidianConfig } from "./obsidianConfig";

export class RequestFormState {
    obsidianConfig: ObsidianConfig = new ObsidianConfig();

    wadName: string = '';
    sanitizedWadName: string = '';
    isInvaldWad: boolean = true;
    takenWadNames: string[] = [];
    wadNameInfoBanner: string = '';
}