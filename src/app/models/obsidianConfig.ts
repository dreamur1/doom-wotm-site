export class ObsidianConfig {
    wadName: string = '';
    formType: string = 'basic';
    deletionTimestamp: string = '';

    /* ---- Game Settings ----- */
    sourceWad: string = 'doom2';
    gameLength: string = 'game';

    /* ---- Architecture ----- */
    levelSize: number = 100;        // multiply by 0.36 to convert
    allowPrebuiltLevels: boolean = true;
    lightingMultiplier: number = 100;   // divide by 100 to convert
    outdoorsFrequency: string = 'mixed';
    cavesFrequency: string = 'mixed';
    liquidsFrequency: string = 'mixed';
    hallwaysFrequency: string = 'mixed';
    teleportersFrequency: string = 'mixed';
    enableBottomlessVistasAndSkyboxes: boolean = false; // skybox mode is set to random in backend

    /* ---- Combat ----- */
    monsterQuantity: number = 100;  // divide by 100 to convert
    monsterStrength: number = 100;  // divide by 100 to convert
    pistolStartCompatable: boolean = true;
    safeStartingRooms: boolean = false;
    monsterVariety: string = 'mixed';
    trapQuantity: string = 'mixed';
    monstersInSecrets: boolean = false;

    /* ---- Pickups ----- */
    healthPickupQuantity: string = 'normal';
    ammoPickupQuantity: string = 'normal';
    miscItemQuantity: string = 'normal';
    secretsQuantity: string = 'mixed';
    secretsBonus: string = 'none';

    /* ---- Other Modules ---- */
    zombieCount: number = 5;
    shotgunnerCount: number = 5;
    sargeCount: number = 5;
    naziCount: number = 5;
    impCount: number = 5;
    lostSoulCount: number = 5;
    demonCount: number = 5;
    spectreCount: number = 5;
    painElementalCount: number = 5;
    cacodemonCount: number = 5;
    hellKnightCount: number = 5;
    revenantCount: number = 5;
    mancubusCount: number = 5;
    arachnatronCount: number = 5;
    archVileCount: number = 5;
    baronOfHellCount: number = 5;
    cyberDemonCount: number = 5;
    spiderMastermindCount: number = 5;
}