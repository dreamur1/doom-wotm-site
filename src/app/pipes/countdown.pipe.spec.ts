import { CountdownPipe } from './countdown.pipe';

describe('CountdownPipe', () => {
  it('create an instance', () => {
    const pipe = new CountdownPipe();
    expect(pipe).toBeTruthy();
  });

  it('should correctly transform datetime', () => {
    const pipe = new CountdownPipe();
    
    let currentTime = new Date().getTime();
    const expired = new Date((currentTime - (1000 * 30))).toISOString();
    expect(pipe.transform(expired)).toBe("Expired");

    const expires30seconds = new Date((currentTime + (1000 * 30))).toISOString();
    expect(pipe.transform(expires30seconds)).toContain("seconds remaining");

    currentTime = new Date().getTime();
    const expires7Minutes = new Date((currentTime + (1000 * 60 * 7))).toISOString();
    expect(pipe.transform(expires7Minutes)).toContain("minutes remaining");

    currentTime = new Date().getTime();
    const expires3hours = new Date((currentTime + (1000 * 60 * 65 * 3))).toISOString();
    expect(pipe.transform(expires3hours)).toContain("hours remaining");
  });
});
