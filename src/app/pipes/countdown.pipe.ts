import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'countdown'
})
export class CountdownPipe implements PipeTransform {

  transform(value: string): any {
    let ms = Date.parse(value) - Date.now();
    let sec = ms / 1000;
    let min = sec / 60;
    let hr = min / 60;
    let days = hr / 24;

    if (days >= 1) {
      return Math.floor(days) > 1 ? `${Math.floor(days)} days remaining` : '1 day remaining'; 
    }

    if (hr >= 1) {
      return Math.floor(hr) > 1 ? `${Math.floor(hr)} hours remaining` : '1 hour remaining';
    }

    if (min >= 1) {
      return Math.floor(min) > 1 ? `${Math.floor(min)} minutes remaining` : '1 minute remaining';
    }

    if (sec > 0) {
      return `${Math.floor(sec)} seconds remaining`;
    }

    return 'Expired';
  }

}
