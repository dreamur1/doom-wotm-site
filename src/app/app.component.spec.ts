import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { LandingComponent } from './components/landing/landing.component';
import { GalleriaModule } from 'primeng/galleria';

describe('AppComponent', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [
      AppComponent,
      LandingComponent
    ],
    imports: [
      GalleriaModule,
      ToastModule
    ],
    providers: [MessageService],
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'doom-wotm-site'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('doom-wotm-site');
  });
});
