import { Component } from '@angular/core';

@Component({
  selector: 'landing',
  templateUrl: './landing.component.html'
})
export class LandingComponent {
  images: any[] = [
    {
      itemImageSrc: './assets/screen1.jpg',
      thumbnailImageSrc: './screen1.jpg',
      alt: 'Description for Image 1',
      title: 'Title 1'
    },
    {
      itemImageSrc: './assets/screen2.jpg',
      thumbnailImageSrc: './screen2.jpg',
      alt: 'Description for Image 2',
      title: 'Title 2'
    },
    {
      itemImageSrc: './assets/screen3.jpg',
      thumbnailImageSrc: './screen3.jpg',
      alt: 'Description for Image 3',
      title: 'Title 3'
    },
    {
      itemImageSrc: './assets/screen4.jpg',
      thumbnailImageSrc: './screen4.jpg',
      alt: 'Description for Image 4',
      title: 'Title 4'
    },
    {
      itemImageSrc: './assets/screen5.jpg',
      thumbnailImageSrc: './screen5.jpg',
      alt: 'Description for Image 5',
      title: 'Title 5'
    },
    {
      itemImageSrc: './assets/screen6.jpg',
      thumbnailImageSrc: './screen6.jpg',
      alt: 'Description for Image 6',
      title: 'Title 6'
    },
  ]
}
