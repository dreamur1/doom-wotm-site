import { Component, EventEmitter, Output } from '@angular/core';
import { map } from 'rxjs';
import { WadData, WotmService } from 'src/app/services/wotm-service/wotm.service';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
})
export class HeaderComponent {
  @Output() onTabSelect = new EventEmitter<string>();
  activeTab: string = 'landing';
  newDownloads: number = 0;
  seenDownloads: number = 0;

  constructor(private wotmService: WotmService) {
    this.wotmService.formType$.pipe(
      map((newFormType: string) => {
        this.updateAndEmitActiveTab('wadForm');
      })
    ).subscribe();

    WotmService.availableWadNames$.pipe(
      map((wads: WadData[]) => {
        if (this.activeTab === 'downloads') {
          this.seenDownloads += 1;
        } else {
          this.newDownloads = wads.length - this.seenDownloads;
        }
      })
    ).subscribe();
  }

  updateAndEmitActiveTab(tabName: string): void {
    if (tabName === 'downloads') { 
      this.seenDownloads += this.newDownloads; 
      this.newDownloads = 0; 
    }
    this.activeTab = tabName;
    this.onTabSelect.emit(tabName);
  }
}

