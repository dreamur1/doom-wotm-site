import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { HttpClientModule } from '@angular/common/http';
import { BadgeModule } from 'primeng/badge';
import { WotmService } from 'src/app/services/wotm-service/wotm.service';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      imports: [
        HttpClientModule,
        BadgeModule,        
      ],
    });
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update active tab & rest of state', () => {
    const html = fixture.debugElement.nativeElement;

    expect(component.activeTab).toBe("landing");
    expect(html.querySelector('.active-header-tab').textContent).toBe('About');

    component.activeTab = "wadForm";
    fixture.detectChanges();
    expect(html.querySelector('.active-header-tab').textContent).toBe('Make a WAD'); 

    component.activeTab = "downloads";
    fixture.detectChanges();

    // the primeNg badge appends the "newDownloads" value to the end of the content behind-the-scenes
    if (component.newDownloads > 0) {
      expect(html.querySelector('.active-header-tab').textContent).toBe(`Downloads${component.newDownloads}`);
    } else {
      expect(html.querySelector('.active-header-tab').textContent).toBe('Downloads');
    }
  });
});
