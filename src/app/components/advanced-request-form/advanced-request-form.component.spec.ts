import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedRequestFormComponent } from './advanced-request-form.component';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { KnobModule } from 'primeng/knob';
import { SelectButtonModule } from 'primeng/selectbutton';
import { TabViewModule } from 'primeng/tabview';
import { SliderModule } from 'primeng/slider';
import { CheckboxModule } from 'primeng/checkbox';

describe('AdvancedRequestFormComponent', () => {
  let component: AdvancedRequestFormComponent;
  let fixture: ComponentFixture<AdvancedRequestFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdvancedRequestFormComponent],
      imports: [    
        FormsModule,
        HttpClientModule, 
        KnobModule,
        InputTextModule,
        SelectButtonModule,
        ButtonModule,
        TabViewModule,
        SliderModule,
        CheckboxModule,
      ],
      providers: [MessageService],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    });
    fixture = TestBed.createComponent(AdvancedRequestFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  xit('::updateSliderIndexes() should set slider vals', () => {
    // todo

    component.updateSliderIndexes();
  });
});
