import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { RequestFormState } from 'src/app/models/requestFormState';

@Component({
  selector: 'advanced-request-form',
  templateUrl: './advanced-request-form.component.html',
})
export class AdvancedRequestFormComponent implements OnChanges {
  @Input() formState: RequestFormState = new RequestFormState();

  @Output() resetConfig: EventEmitter<void> = new EventEmitter();
  @Output() sanitizeWad: EventEmitter<void> = new EventEmitter();
  @Output() submitForm: EventEmitter<void> = new EventEmitter();

  gameLengthIndex: number = 3;
  secretsPerLvlIndex: number = 7;
  bonusInSecretsIndex: number = 0;

  healthPickupIndex: number = 4;
  ammoPickupIndex: number = 4;
  itemPickupIndex: number = 3;

  outdoorFreqIndex: number = 7;
  cavesFreqIndex: number = 7;
  liquidPoolsFreqIndex: number = 7;
  hallwaysFreqIndex: number = 7;
  teleportersFreqIndex: number = 7;

  monVarietyInded: number = 7;
  trapFreqIndex: number = 7;
  
  doomWadOptions: any[] = [
    {label: 'Doom', value: 'doom1'}, 
    {label: 'Ultimate Doom', value: 'ultdoom'},
    {label: 'Doom 2', value: 'doom2'},
    {label: 'TNT', value: 'tnt'},
    {label: 'Plutonia', value: 'plutonia'}
  ];
  gameLengthOptions: any[] = [
    {label: 'Single Level', value: 'single'}, 
    {label: 'Four Maps', value: 'few'},
    {label: 'Single Episode', value: 'episode'},
    {label: 'Full Game', value: 'game'}
  ];
  healthAndAmmoOptions: any[] = [
    {label: 'None', value: 'none'}, 
    {label: 'Scarce', value: 'scarce'},
    {label: 'Less', value: 'less'},
    {label: 'Bit Less', value: 'bit_less'},
    {label: 'Normal', value: 'normal'},
    {label: 'Bit More', value: 'bitMore'},
    {label: 'More', value: 'more'},
    {label: 'Heaps', value: 'heaps'}
  ];
  itemFreqOptions: any[] = [
    {label: 'None', value: 'none'}, 
    {label: 'Rare', value: 'rare'},
    {label: 'Less', value: 'less'},
    {label: 'Normal', value: 'normal'},
    {label: 'More', value: 'more'},
    {label: 'Heaps', value: 'heaps'}
  ];
  secretsBonusOptions: any[] = [
    {label: 'None', value: 'none'}, 
    {label: 'More', value: 'more'},
    {label: 'Heaps', value: 'heaps'},
    {label: 'Rich', value: 'heapser'},
    {label: 'Resplendant', value: 'heapsest'}
  ];
  noneRareFewLessSomeMoreHeaps: any[] = [
    {label: 'None', value: 'none'}, 
    {label: 'Rare', value: 'rare'},
    {label: 'Few', value: 'few'},
    {label: 'Less', value: 'less'},
    {label: 'Some', value: 'some'},
    {label: 'More', value: 'more'},
    {label: 'Heaps', value: 'heaps'}
  ];
  nRFLSMHandMixed: any[] = [...this.noneRareFewLessSomeMoreHeaps, {label: 'Random per Level', value: 'mixed'}];

  ngOnChanges(changes: SimpleChanges): void {
    this.updateSliderIndexes();
  }

  requestNewWad(): void {
    this.submitForm.emit()
  }

  sanitizeWadName(): void {
    this.sanitizeWad.emit()
  }

  resetToDefaults(): void {
    this.resetConfig.emit();
  }

  updateSliderIndexes(): void {
    this.gameLengthIndex = this.gameLengthOptions.findIndex(option => option.value === this.formState.obsidianConfig.gameLength);
    this.secretsPerLvlIndex = this.nRFLSMHandMixed.findIndex(option => option.value === this.formState.obsidianConfig.secretsQuantity);
    this.bonusInSecretsIndex = this.secretsBonusOptions.findIndex(option => option.value === this.formState.obsidianConfig.secretsBonus);
    this.healthPickupIndex = this.healthAndAmmoOptions.findIndex(option => option.value === this.formState.obsidianConfig.healthPickupQuantity);
    this.ammoPickupIndex = this.healthAndAmmoOptions.findIndex(option => option.value === this.formState.obsidianConfig.ammoPickupQuantity);
    this.itemPickupIndex = this.itemFreqOptions.findIndex(option => option.value === this.formState.obsidianConfig.miscItemQuantity);
    this.outdoorFreqIndex = this.nRFLSMHandMixed.findIndex(option => option.value === this.formState.obsidianConfig.outdoorsFrequency);
    this.cavesFreqIndex = this.nRFLSMHandMixed.findIndex(option => option.value === this.formState.obsidianConfig.cavesFrequency);
    this.liquidPoolsFreqIndex = this.nRFLSMHandMixed.findIndex(option => option.value === this.formState.obsidianConfig.liquidsFrequency);
    this.hallwaysFreqIndex = this.nRFLSMHandMixed.findIndex(option => option.value === this.formState.obsidianConfig.hallwaysFrequency);
    this.teleportersFreqIndex = this.nRFLSMHandMixed.findIndex(option => option.value === this.formState.obsidianConfig.teleportersFrequency);
    this.monVarietyInded = this.nRFLSMHandMixed.findIndex(option => option.value === this.formState.obsidianConfig.monsterVariety);
    this.trapFreqIndex = this.nRFLSMHandMixed.findIndex(option => option.value === this.formState.obsidianConfig.trapQuantity);
  }

  updateGameLength(): void {
    this.formState.obsidianConfig.gameLength = this.gameLengthOptions[this.gameLengthIndex].value;
  }
  updateSecretsPerLevel(): void {
    this.formState.obsidianConfig.secretsQuantity = this.nRFLSMHandMixed[this.secretsPerLvlIndex].value;
  }
  updateBonusLoot(): void {
    this.formState.obsidianConfig.secretsBonus = this.secretsBonusOptions[this.bonusInSecretsIndex].value;
  }
  updateHealthPickupFreq(): void {
    this.formState.obsidianConfig.healthPickupQuantity = this.healthAndAmmoOptions[this.healthPickupIndex].value;
  }
  updateAmmoPickupFreq(): void {
    this.formState.obsidianConfig.ammoPickupQuantity = this.healthAndAmmoOptions[this.ammoPickupIndex].value;
  }
  updateItemPickupFreq(): void {
    this.formState.obsidianConfig.miscItemQuantity = this.itemFreqOptions[this.itemPickupIndex].value;
  }

  udpateOutdoorsFreq(): void {
    this.formState.obsidianConfig.outdoorsFrequency = this.nRFLSMHandMixed[this.outdoorFreqIndex].value;
  }
  updateCavesFreq(): void {
    this.formState.obsidianConfig.cavesFrequency = this.nRFLSMHandMixed[this.cavesFreqIndex].value;
  }
  updateLiquidsFreq(): void {
    this.formState.obsidianConfig.liquidsFrequency = this.nRFLSMHandMixed[this.liquidPoolsFreqIndex].value;
  }
  updateHallwaysFreq(): void {
    this.formState.obsidianConfig.hallwaysFrequency = this.nRFLSMHandMixed[this.hallwaysFreqIndex].value;
  }
  updateTeleportersFreq(): void {
    this.formState.obsidianConfig.teleportersFrequency = this.nRFLSMHandMixed[this.teleportersFreqIndex].value;
  }

  updateMonVariety(): void {
    this.formState.obsidianConfig.monsterVariety = this.nRFLSMHandMixed[this.monVarietyInded].value;
  }
  updateTrapFreq(): void {
    this.formState.obsidianConfig.trapQuantity = this.nRFLSMHandMixed[this.trapFreqIndex].value;
  }  
}
