import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvailableWadsComponent } from './available-wads.component';
import { HttpClientModule } from '@angular/common/http';
import { CountdownPipe } from 'src/app/pipes/countdown.pipe';

describe('AvailableWadsComponent', () => {
  let component: AvailableWadsComponent;
  let fixture: ComponentFixture<AvailableWadsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AvailableWadsComponent, CountdownPipe],
      imports: [
        HttpClientModule,
      ],
    });
    fixture = TestBed.createComponent(AvailableWadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
