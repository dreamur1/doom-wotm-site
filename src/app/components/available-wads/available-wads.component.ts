import { Component } from '@angular/core';
import { combineLatest, map } from 'rxjs';
import { WadData, WotmService } from 'src/app/services/wotm-service/wotm.service';
import { environment } from 'src/environments/environment';

interface PageEvent {
  first: number;
  rows: number;
  page: number;
  pageCount: number;
}

@Component({
  selector: 'available-wads',
  templateUrl: './available-wads.component.html',
})
export class AvailableWadsComponent {
  environmentConfig = environment;

  availableWads: WadData[] = [];
  wotms: WadData[] = [];

  constructor(private wotmService: WotmService) {
    combineLatest([WotmService.wotms$, WotmService.availableWadNames$]).pipe(
      map((wadData: any[]) => {
        this.wotms = wadData[0];
        this.availableWads = wadData[1];
      })
    ).subscribe();
  }

  isExpired(dateString: string): boolean {
    return dateString < new Date().toISOString();
  }

  fetchConfig(targetWad: string, isWotm: boolean = false) {
    this.wotmService.fetchConfig(targetWad, isWotm);
  }
}
