import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicRequestFormComponent } from './basic-request-form.component';
import { HttpClientModule } from '@angular/common/http';
import { MessageService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { KnobModule } from 'primeng/knob';
import { SelectButtonModule } from 'primeng/selectbutton';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ObsidianConfig } from 'src/app/models/obsidianConfig';

describe('BasicRequestFormComponent', () => {
  let component: BasicRequestFormComponent;
  let fixture: ComponentFixture<BasicRequestFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BasicRequestFormComponent],
      imports: [    
        FormsModule,
        HttpClientModule, 
        KnobModule,
        InputTextModule,
        SelectButtonModule,
        ButtonModule,
      ],
      providers: [MessageService],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    });
    fixture = TestBed.createComponent(BasicRequestFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render basic layout with defaults', () => {
    const html = fixture.debugElement.nativeElement;

    const basicLevelSizeKnobWrapper = html.querySelector('.basic-level-size-knob>.knob-wrapper');
    expect(basicLevelSizeKnobWrapper.querySelector('.knob-label').textContent).toContain('Level Size');
    expect(basicLevelSizeKnobWrapper.querySelector('p-knob>div>svg>text').textContent).toBe("100%");

    const monsterKnobsColumn = html.querySelector('.basic-monster-knobs');
    expect(monsterKnobsColumn.querySelector('.knob-wrapper:first-of-type>.knob-label').textContent).toContain('Monster Quantity');
    expect(monsterKnobsColumn.querySelector('.knob-wrapper:first-of-type>p-knob>div>svg>text').textContent).toBe("100%");
    expect(monsterKnobsColumn.querySelector('.knob-wrapper:last-of-type>.knob-label').textContent).toContain('Monster Strength');
    expect(monsterKnobsColumn.querySelector('.knob-wrapper:last-of-type>p-knob>div>svg>text').textContent).toBe("100%");

    const buttonTray = html.querySelector('.basic-button-tray-buttons');
    expect(buttonTray.querySelector('div:nth-child(1)>.button-select-label').textContent).toContain('Game Length');
    expect(buttonTray.querySelector('div:nth-child(2)>.button-select-label').textContent).toContain('Health Pickup Quantity');
    expect(buttonTray.querySelector('div:nth-child(3)>.button-select-label').textContent).toContain('Ammo Pickup Quantity');

    expect(component.formState.obsidianConfig).toEqual(new ObsidianConfig());
  });
});
