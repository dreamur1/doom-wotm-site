import { Component, EventEmitter, Input, Output } from '@angular/core';
import { RequestFormState } from 'src/app/models/requestFormState';

@Component({
  selector: 'basic-request-form',
  templateUrl: './basic-request-form.component.html'
})
export class BasicRequestFormComponent {
  @Input() formState: RequestFormState = new RequestFormState();

  @Output() resetConfig: EventEmitter<void> = new EventEmitter();
  @Output() sanitizeWad: EventEmitter<void> = new EventEmitter();
  @Output() submitForm: EventEmitter<void> = new EventEmitter();

  gameLengthOptions: any[] = [
    {label: 'Single Level', value: 'single'}, 
    {label: 'Four Maps', value: 'few'},
    {label: 'Single Episode', value: 'episode'},
    {label: 'Full Game', value: 'game'}
  ];

  healthAndAmmoOptions: any[] = [
    {label: 'None', value: 'none'}, 
    {label: 'Scarce', value: 'scarce'},
    {label: 'Less', value: 'less'},
    {label: 'Bit Less', value: 'bit_less'},
    {label: 'Normal', value: 'normal'},
    {label: 'Bit More', value: 'bitMore'},
    {label: 'More', value: 'more'},
    {label: 'Heaps', value: 'heaps'}
  ];

  requestNewWad(): void {
    this.submitForm.emit()
  }

  sanitizeWadName(): void {
    this.sanitizeWad.emit()
  }

  resetToDefaults(): void {
    this.resetConfig.emit();
  }
}
