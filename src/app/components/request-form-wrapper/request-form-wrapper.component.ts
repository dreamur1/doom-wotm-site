import { Component } from '@angular/core';
import { MessageService } from 'primeng/api';
import { map } from 'rxjs';
import { ObsidianConfig } from 'src/app/models/obsidianConfig';
import { RequestFormState } from 'src/app/models/requestFormState';
import { WadData, WotmService } from 'src/app/services/wotm-service/wotm.service';

@Component({
  selector: 'request-form-wrapper',
  templateUrl: './request-form-wrapper.component.html'
})
export class RequestFormWrapperComponent {
  isAdvancedMode: boolean = false;
  formState = new RequestFormState();

  constructor(private wotmService: WotmService,
    private messageService: MessageService) {
      this.wotmService.retrievedConfig$.pipe(
        map((newConfigObject: ObsidianConfig) => {
          this.formState.obsidianConfig = newConfigObject;
          this.formState.wadName = newConfigObject.wadName;
          this.formState.sanitizedWadName = newConfigObject.wadName;
          this.formState.isInvaldWad = false;
          this.isAdvancedMode = newConfigObject.formType !== 'basic';
        }
      )).subscribe();
      WotmService.availableWadNames$.pipe(map((data: WadData[]) => this.formState.takenWadNames = data.map((wadData: WadData) => wadData.wadName) )).subscribe();
  }

  requestNewWad(): void {
    this.sanitizeWadName();
    this.wotmService.submitWadRequest(this.formState.sanitizedWadName, this.formState.obsidianConfig);
    this.messageService.add({ severity: 'success', summary: 'WAD Request Submitted', detail: `"${this.formState.wadName}" has been added to the work queue!` });
    this.resetToDefaults();
    this.isAdvancedMode = false;
  }

  sanitizeWadName(): void {
    this.formState.wadNameInfoBanner = '';
    this.formState.isInvaldWad = false;

    this.formState.sanitizedWadName = this.formState.wadName.replaceAll(' ', '-').replace(/[^0-9a-zA-Z-]/g, '');
    if (this.formState.sanitizedWadName !== this.formState.wadName) {
      this.formState.wadNameInfoBanner = `File name will be: ${this.formState.sanitizedWadName}`;
    }

    this.formState.isInvaldWad = this.formState.sanitizedWadName.trim() === '';
    if (this.formState.takenWadNames.includes(this.formState.sanitizedWadName)) {
      this.formState.isInvaldWad = true;
      this.formState.wadNameInfoBanner = `"${this.formState.sanitizedWadName}" already exists! Please try a different name.`;
    }

    if (this.formState.sanitizedWadName.length > 25) {
      this.formState.isInvaldWad = true;
      this.formState.wadNameInfoBanner = `WAD name too long!`;
    }
  }

  resetToDefaults(): void {
    let tmpTakenWads = this.formState.takenWadNames;
    this.formState = new RequestFormState();
    this.formState.takenWadNames = tmpTakenWads;
    this.formState.obsidianConfig.formType = this.isAdvancedMode ? 'advanced' : 'basic';
  }
}
