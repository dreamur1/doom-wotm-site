import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestFormWrapperComponent } from './request-form-wrapper.component';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FormsModule } from '@angular/forms';
import { BasicRequestFormComponent } from '../basic-request-form/basic-request-form.component';
import { HttpClientModule } from '@angular/common/http';
import { MessageService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { KnobModule } from 'primeng/knob';
import { SelectButtonModule } from 'primeng/selectbutton';
import { ObsidianConfig } from 'src/app/models/obsidianConfig';

describe('RequestFormWrapperComponent', () => {
  let component: RequestFormWrapperComponent;
  let fixture: ComponentFixture<RequestFormWrapperComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        BasicRequestFormComponent, 
        RequestFormWrapperComponent
      ],
      providers: [
        MessageService
      ],
      imports: [
        FormsModule, 
        InputSwitchModule,
        HttpClientModule,
        ButtonModule,
        KnobModule,
        SelectButtonModule,
      ]
    });
    fixture = TestBed.createComponent(RequestFormWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

 it('::sanitizeWadName() should prevent invalid wad names', () => {
    const html = fixture.debugElement.nativeElement;
    expect(html.querySelector('.corrected-wad-name')).toBeFalsy();

    component.formState.obsidianConfig.wadName = '%<potato<<.*';
    component.formState.wadName = component.formState.obsidianConfig.wadName;
    component.sanitizeWadName();
    let sanitizedWadName = component.formState.obsidianConfig.wadName.replaceAll(' ', '-').replace(/[^0-9a-zA-Z-]/g, '');
    let expectedMessage = `File name will be: ${sanitizedWadName}`;
    expect(component.formState.wadNameInfoBanner).toBe(expectedMessage);

    component.formState.takenWadNames = ['test'];
    component.formState.obsidianConfig.wadName = component.formState.takenWadNames[0];
    component.formState.wadName = component.formState.obsidianConfig.wadName;
    component.sanitizeWadName();
    expectedMessage = `"${component.formState.takenWadNames[0]}" already exists! Please try a different name.`;
    expect(component.formState.wadNameInfoBanner).toBe(expectedMessage);
    
    component.formState.obsidianConfig.wadName = '';
    component.formState.wadName = component.formState.obsidianConfig.wadName;
    component.sanitizeWadName();
    expect(component.formState.wadNameInfoBanner).toBe('');

    component.formState.obsidianConfig.wadName = '123456789012345678901234567890';
    component.formState.wadName = component.formState.obsidianConfig.wadName;
    component.sanitizeWadName();
    expectedMessage = `WAD name too long!`;
    expect(component.formState.wadNameInfoBanner).toBe(expectedMessage);
  });

  it('::resetToDefaults() should set default vals', () => {
    component.formState.obsidianConfig.wadName = "test";
    component.formState.sanitizedWadName = "test";
    component.formState.obsidianConfig.levelSize = 229;
    component.isAdvancedMode = true;
    
    let expectedObsidianConfig = new ObsidianConfig();
    expectedObsidianConfig.formType = 'advanced';

    component.resetToDefaults();

    expect(component.formState.sanitizedWadName).toBe('');
    expect(component.formState.wadNameInfoBanner).toBe('');
    expect(component.isAdvancedMode).toBeTrue();
    expect(component.formState.obsidianConfig).toEqual(expectedObsidianConfig);
  });
});
